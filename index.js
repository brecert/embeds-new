import express from "express";
import { metaPage } from "./utils/metaPage";
import { randomColor } from "./utils/randomColor";
import UnicodeTools from "./utils/UnicodeTools";
import fetch from "node-fetch";

import { CommandClient } from "camdo";
const app = express();
const cache = {};

app.use(express.static("public"));

app.get("/embed", (req, res) => {});

function getAll(endpoints, opts) {
  let promises = [];
  endpoints.map((endpoint, i) => {
    promises.unshift(
      fetch(endpoint).then(res => (opts.json ? res.json() : res.text()))
    );
  });
  return Promise.all(promises);
}

function generateUrl(id, args) {
  const io = [];
  args.forEach(arg => {
    let oo = "";
    oo += ":" + arg.id;
    if (!arg.required) oo += "?";
    io.push(oo);
  });

  const url = `/${id}/${io.join("/")}`;
  console.log(url);
  return url;
}

const inOut = opts => {
  console.log("out:", opts);
  const out = {
    "og:type": "website"
  };
  if ("name" in opts) out["og:site_name"] = opts.name;
  if ("title" in opts) out["og:title"] = opts.title;
  if ("description" in opts) out["og:description"] = opts.description;
  if ("image" in opts) out["og:image"] = opts.image;
  if ("color" in opts) out["theme-color"] = randomColor(opts.color);
  if ("width" in opts) out["og:image:width"] = opts.width;
  if ("height" in opts) out["og:image:height"] = opts.height;
  if (opts.format === "large_image")
    out["twitter:card"] = "summary_large_image";

  return out;
};

const client = new CommandClient();

client.definitionTemplate = function(cmd, cb) {
  app.get(generateUrl(cmd.id, cmd.args), async (req, res) => {
    res.set('Cache-Control', 'must-revalidate')
    this.send = data => res.send(metaPage(inOut(data)));
    cb({ ...req.params, req: res });
  });
};

const loadCommand = fileName =>
  import(`./src/commands/${fileName}`)
    .then(({ default: define }) => define(client))
    .catch(err => {
      throw new Error(
        `CommandError: LoadError: there was a problem loadinging \`${fileName}\`\n${err}`
      );
    });

import { readdir } from "fs-extra";
const loadCommands = () =>
  readdir("./src/commands").then(fileNames =>
    fileNames.forEach(fileName => loadCommand(fileName))
  );

loadCommands();

const listener = app.listen(process.env.PORT, () => {
  console.log("Your app is listening on port " + listener.address().port);
});
