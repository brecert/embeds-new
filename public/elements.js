import {html} from '//unpkg.com/lighterhtml?module'

customElements.define('form-input', class extends HTMLElement {
  constructor() {
    super(...arguments);
    this.state = {title: this.getAttribute("title"), name: this.getAttribute("key")};
    
    let updators = this.querySelectorAll('[auto_update_form="true"]')
    
    updators.forEach(el => {
      el.addEventListener('input', event => {
        let target = document.getElementById(this.state.name)
        target.value = event.target.value
        target.dispatchEvent(new InputEvent('input', event))
      })
    })
    
    this.addEventListener('input', event => {
      updators.forEach(el => {
        el.value = event.target.value
      })
    })
    
    this.prepend(this.render())
  }
  render() {
    const {title, name} = this.state;
    
    return html`
      <div class="field-item">
        <label for=${name}>${title}</label>
        <textarea name=${name} id=${name} maxlength="256"></textarea>
      </div>`;
  }
});