import {render, html} from '//unpkg.com/lighterhtml?module';
const BASE_URL = window.location.href + 'embed/'
const options = {}

const PreviewSiteName = (siteName) => html`
    <div class="embed-provider-container">
      <span class="embed-provider size-12 weight-normal">${siteName}</span>
    </div>`

const PreviewTitle = (title) => html`
    <div class="embed-margin margin-top">
      <a class="embed-title-link embed-link embed-title size-14 weight-medium" target="_blank" rel="noreferrer noopener" href="">${title}</a>
    </div>`

const PreviewDescription = (description) => html`<div class="embed-description markup">${description}</div>`

const EmbedPreview = (options = {}) => html`
    <div class="embed embed-container">
      <div style=${options["theme-color"] ? `background: ${options["theme-color"]}` : "" } class="embed-pill"></div>
      <div class="embed-inner">
        <div class="embed-content">
          <div id="embed-description" class="embed-content-inner">
            ${options["og:site_name"] ? PreviewSiteName(options["og:site_name"]) : ""}
            ${options["og:title"] ? PreviewTitle(options["og:title"]) : ""}
            ${options["og:description"] ? PreviewDescription(options["og:description"]) : ""}
          </div>
        </div>
      </div>
    </div>`


let form = document.getElementById('info_form')
form.addEventListener('input', update, false)

const inputs = document.querySelectorAll('form [name]');
for(let input of inputs) {
  input.setAttribute('style', 'height:' + (input.scrollHeight) + 'px;overflow-y:hidden;');
}

const output = document.getElementById("output")

function generateUrl() {
  return `${BASE_URL}?${addQuery(options)}`
}
  
function addQuery(obj) {
  return Object.keys(obj).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`).join('&');
}

function update(event) {
    
  let target = event.target
  
  if(target.type === "textarea") {
    target.style.height = 'auto';
    target.style.height = (target.scrollHeight) + 'px';
  }
  
  if(target.value && target.name){
    options[target.name] = target.value
  } else {
    delete options[target.name]
  }
  
  output.value = generateUrl()
  
  console.log(options)
  
  render(accessories, () => EmbedPreview(options))
}

const accessories = document.getElementById("accessories")
render(accessories, () => EmbedPreview(options))