interface GenericAttatchment {
  name?: string
  
  /**
   * file uri, if url is not possible, please convert to b64 data
   */
  file?: string

  type?: 'image'
  
  image_width: number
  image_height: number
}

interface GenericEmbed {
  title?: string
  subtitle?: string
  description?: string
  attatchments?: GenericAttatchment[]
  color?: number
}

interface Displayable {
  name: string | string[]
  description: string
}

export interface TypeDefinition implements Displayable {
  name: string
  description: string
  validate(arg: string): boolean
  transform(arg: string): any
}

export interface ArgumentDefinition implements Displayable {
  name: string[]
  description: string
  type: TypeDefinition
  match: (args: string[]) => { match: string[], modified: string[] }
}

export interface CommandDefiniton implements Displayable {
  name: string[]
  description: string
  args: ArgumentDefinition[]
  run(args: { [name: string]: any }): 
}

export interface HandlerDefinition implements Displayable {
  name: string
  description: string
  parse(cb: (results: string[], ...passedData: any[]) => boolean | any): any
  send(args: string[], ...passedData: any[]): any
}

/*

cmd.addHandler(handle => {
  app.on('**', (req, res) => {
    const result = handle(req.url.split('/')).catch(err => res.send(`error: ${err}`))
    if(result) {
      res.send(translateResultToHtml(result))
    }
  })
})

function parse(cb) {

  on event then
    split event.target.value and 
    call cb with the split values
}

*/

export interface Camdo {
  commands: CommandDefinition[]
  addCommand(command: CommandDefinition): typeof this
  addHandler(handle: (...args: string[]) => any): typeof this
}