import fetch from 'node-fetch'
export default function getAll(endpoints, opts) {
  let promises = [];
  endpoints.map((endpoint, i) => {
    promises.unshift(
      fetch(endpoint).then(res => opts.json ? res.json() : res.text())
    );
  });
  return Promise.all(promises);
}