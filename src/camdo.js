/*
export interface TypeDefinition implements Displayable {
  name: string
  description: string
  validate(arg: string): boolean
  transform(arg: string): any
}

export interface ArgumentDefinition implements Displayable {
  name: string[]
  description: string
  type: TypeDefinition
  match: (args: string[]) => { match: string[], modified: string[] }
}

export interface CommandDefiniton implements Displayable {
  name: string[]
  description: string
  args: ArgumentDefinition[]
  run(args: { [name: string]: any }): 
}

*/
class Camdo {
  constructor(commands = []) {
    this.commands = commands;
  }

  addCommand(command) {
    this.commands.push(command);
    return this;
  }

  
  parseArguments(args) {
    this.commands.forEach(command => {
      let i = 0;
      for(; i < command.args.length; i++) {
        const arg = command.args[i]
        const matched = arg.match(args)
        matched.filter(a => a.validate(arg))
      }
    })
  }
  
  /**
   * cmd.addHandler(handle => {
   *   app.on('**', (req, res) => {
   *     const result = handle(req.url.split('/')).catch(err => res.send(`error: ${err}`))
   *     if(result) {
   *       res.send(translateResultToHtml(result))
   *     }
   *   })
   * })
   */
  addHandler(handler) {
    new Promise((res, rej) => handler(res)).then(args => {
      
    })
    return this;
  }
}
