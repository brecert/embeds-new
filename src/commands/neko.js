import getAll from "../utils/getAll.js";

const neko_list = [
  "pat",
  "hug",
  "smug",
  "slap",
  "poke",
  "neko",
  "woof",
  "meow",
  "kiss",
  "feed",
  "ngif",
  "waifu",
  "8ball",
  "tickle",
  "cuddle",
  "avatar",
  "fox_girl"
];

export default function(client) {
  client.defineType({
    id: "neko",
    display: neko_list.join(", "),
    validate: val => neko_list.includes(val)
  });

  client.defineCommand({
    id: "neko",
    desc: "get a neko! :3",
    examples: ["https://embeds.glitch.com/neko/pat"],
    args: [
      {
        id: "neko_type",
        name: "Neko Type",
        desc: "The type of neko you wish to display",
        type: "neko",
        default_value: "neko",
        fail_message: `The neko argument accepts "${client.types["neko"].display}"`,
        required: false
      }
    ],
    async run({ neko_type }) {
      const [cat, img] = await getAll(
        [
          `https://nekos.life/api/v2/img/${neko_type}`,
          "https://nekos.life/api/v2/cat"
        ],
        { json: true }
      );

      return {
        title: cat.cat,
        image: img.url,
        color: 0x975dc4,
        format: "large_image"
      };
    }
  });
}
