function formatArg(arg) {
  let [start, end] = arg.required ? ["<", ">"] : ["[", "]"];
  return `${start}${arg.id}${end}`;
}

export default function(client) {
  // client.defineType({
  //   id: "command",
  //   display: Array.from(Object.keys(client.commands), ([v]) => {
  //     return v;
  //   }).join(", "),
  //   validate: arg => arg in client.commands
  // });

  client.defineCommand({
    id: "help",
    args: [
      {
        id: "command",
        name: "command",
        description: "Help for a specific command",
        required: false
      }
    ],
    run({ command }) {
      // console.log(Array.from(client.commands, ([key, value]) => `${key}: ${JSON.stringify(value)}`))
      if (!command) {
        let commandList = Object.entries(client.commands).map(([id, cmd]) => {
          return `${cmd.id} ${cmd.args.map(formatArg).join(" ")}`;
        })
          .map(cmd => `> ${cmd}`)
          .join("\n");

        return {
          title: "commands",
          description: commandList
        };
      } else {
        return {
          ...client.commands.get(command)
        };
      }
    }
  });
}
