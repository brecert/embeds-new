import fetch from 'node-fetch'

const getLanguages = async () =>
  fetch(
    `https://translate.yandex.net/api/v1.5/tr.json/getLangs?ui=en&key=${process.env.YANDEX}`
  ).then(res => res.json());

export default async function(client) {
  const languages = await getLanguages()
  
  client.defineType({
    id: "lang",
    display: Object.values(languages.dirs)
      .map(k => `${k}`)
      .join("\n"),
    validate: val => languages.dirs.includes(val)
  });

  client.defineCommand({
    id: "translate",
    desc: "translate stuff",
    examples: ["https://embeds.glitch.me/translate/hello+world!/en-ru"],
    args: [
      {
        id: "text",
        name: "text",
        desc: "the text to translate",
        type: "any",
        required: true
      },
      {
        id: "translationDirection",
        name: "translation direction",
        desc: "the translation direction",
        type: "lang",
        required: true
      }
    ],
    async run({ text, translationDirection }) {
      const res = await fetch(`https://translate.yandex.net/api/v1.5/tr.json/translate?text=${text}&lang=${translationDirection}&key=${process.env.YANDEX}`).then(res => res.json());
      console.log(res);
      if (res.code === 200) {
        return {
          name: res.lang,
          description: res.text.join("\n")
        };
      } else {
        return {
          name: `${res.code}`,
          description: res.message
        };
      }
    }
  });
}
