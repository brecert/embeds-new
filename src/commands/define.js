import fetch from "node-fetch";

function dictInfo(dict, word) {
  let info = [];
  info.push(`${dict.fl || word}`);
  if ("prs" in dict.hwi) {
    info.push(`[${dict.hwi.prs[0].mw}]`);
  } else if ("hw" in dict.hwi) {
    info.push(`[${dict.hwi.hw}]`);
  }
  info.push("\n");
  if ("lbs" in dict) info.push(`${dict.lbs.join(", ")}`);

  let desc =
    dict.shortdef.length !== 0
      ? dict.shortdef.join("\n")
      : dict.cxs
          .map(cx => `${cx.cxl} ${cx.cxtis.map(is => is.cxt).join(", ")}`)
          .join(", ");
  
  return { info, desc, title: dict.hwi.hw }
}

export default function(client) {
  client.defineCommand({
    id: "define",
    desc: "get the definition of a word",
    examples: ["https://embeds.glitch.me/define/gaskell"],
    args: [
      {
        id: "word",
        name: "word",
        desc: "The word to define",
        type: "any",
        required: true
      },
      {
        id: "entry",
        name: "definition entry",
        desc: "The definition entry to select",
        type: "any",
        required: false
      }
    ],
    async run({ word, entry = 1 }) {
      entry = Number(entry)
      console.log(entry)
      if(entry === NaN || entry === undefined) {
        return {
          name: `ERROR: Entry must be a number, not ${JSON.stringify(entry)}`
        }
      }
      const api = await fetch(
        `https://dictionaryapi.com/api/v3/references/collegiate/json/${word}?key=e43cf39b-5fc0-4fed-b991-d3adb9b4ee08`
      ).then(res => res.json());
      const dict = api[entry-1];

      if (typeof api[0] === "string") {
        return {
          name: word,
          description: `Could not find ${word}, did you mean?\n${api.join(
            "\n"
          )}`
        };
      }
      
      if(entry > api.length) {
        return {
          name: "ERROR",
          description: `Entry ${entry}/${api.length} is not a valid entry!`
        }
      }
      
      let { info, desc, title } = dictInfo(dict)

      return {
        name: [`${entry}/${api.length}\n`, ...info].join(" "),
        title: `${title}`,
        description: desc,
        color: 0x555555
      };
    }
  });
}
