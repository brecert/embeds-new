import fetch from 'node-fetch'

export default function(client) {
  client.defineCommand({
    id: "r",
    description: "post from subreddit",
    args: [
      {
        id: "subreddit",
        name: "subreddit",
        description: "the subreddit to get the post from",
        type: "any",
        required: true
      },
      {
        id: "nsfw",
        name: "nsfw",
        description: "allow nsfw content?",
        default_value: "",
        type: "any",
        required: false
      }
    ],
    async run({ subreddit, nsfw }) {
      try {
        const res = await randomPost(subreddit);
        console.log(nsfw);
        if (nsfw !== "nsfw" && nsfw !== "true" && res.nsfw) {
          return {
            name: "ERROR",
            title:
              "You can only see nsfw content if you enable it with the nsfw param",
            color: 0x00ff00
          };
        }

        const out = {
          name: `${res.nsfw ? "🚫nsfw " : ""}👍${res.upvotes} 👎${
            res.downvotes
          } 💬${res.comments}`,
          title: res.title,
          description: res.content
        };

        if (res.media_url) {
          out.image = res.media_url;
          out.format = "large_image";
        }

        return out;
      } catch (err) {
        return {
          name: "ERROR",
          title: "There was an error getting a subreddit post",
          description: err.toString(),
          color: 0x00ff00
        };
      }
    }
  });
}
async function randomPost(sub) {
  return await fetch(`https://www.reddit.com/r/${sub}/random/.json`, {
    headers: {
      "user-agent": "embeds"
    }
  })
    .then(res => res.json())
    .then(res => {
      const { children } = !Array.isArray(res) ? res.data : res[0].data;

      // Check if it contains something
      if (children.length < 1) {
        throw new Error("No posts.");
      }

      let { data } = children[0];

      // Check if crossposted
      if (data.crosspost_parent_list) {
        data = data.crosspost_parent_list[0];
      }

      // Parse post
      const {
        title,
        selftext,
        ups,
        downs,
        num_comments,
        url,
        permalink,
        author,
        over_18,
        is_self
      } = data;

      const obj = {
        title,
        content: selftext,
        author,
        upvotes: ups,
        downvotes: downs,
        comments: num_comments,
        nsfw: over_18,
        reddit_url: `https://reddit.com${permalink}`
      };

      // Check if this post contains an image/video/gif
      // TODO: Check more cases
      if (!is_self) {
        // Get proper media url
        if (url.startsWith("https://v.redd.")) {
          obj.media_url = data.media.reddit_video.fallback_url;
        } else if (url.startsWith("https://gfycat.")) {
          const reg = /[^\/]+(?=\/$|$)/;
          const match = reg.exec(url);

          if (match) {
            obj.media_url = `https://giant.gfycat.com/${match[0]}.webm`;
          }
        } else {
          obj.media_url = url;
        }
      }

      return obj;
    });
}
