import * as Booru from "booru";

export default function(client) {
  client.defineType({
    id: "booru",
    display: Object.values(Booru.sites)
      .map(b => `${b.nsfw ? "(NSFW)" : ""} ${b.domain} [${b.aliases}]`)
      .join("\n"),
    validate: val =>
      Object.values(Booru.sites)
        .map(b => b.aliases)
        .some(a => a.includes(val))
  });

  client.defineCommand({
    id: "get",
    description: "get an image",
    args: [
      {
        id: "tags",
        name: "search tags",
        description: "The tags you want",
        type: "any",
        required: false
      },
      {
        id: "booru",
        name: "booru",
        description: "the booru you want to search",
        default_value: "safebooru",
        type: "booru",
        required: false
      }
    ],
    async run({ tags, booru }) {
      const NSFW_TAG = "::nsfw";
      console.log(tags, booru);
      let joinedTags = ["rating:safe"].concat((tags || "").split(","));
      if (joinedTags.some(tag => tag === NSFW_TAG)) {
        joinedTags.shift();
        joinedTags = joinedTags.filter(tag => tag !== NSFW_TAG);
      } else {
        if (
          Object.values(Booru.sites)
            .filter(b => b.nsfw)
            .flatMap(b => b.aliases)
            .includes(booru)
        ) {
          return {
            name: "ERROR",
            title: `You can only search nsfw boorus if you enable it with the nsfw tag \`${NSFW_TAG}\``,
            color: 0x00ff00
          };
        }
      }

      console.log(joinedTags);
      const images = await Booru.search(booru, joinedTags, {
        limit: 1,
        random: true
      });

      if (images[0] === undefined) {
        return {
          name: "error or something",
          title:
            "no result found tell bree to fix this msg am tied she tired help",
          color: 0x00ff00
        };
      }

      const base = {
        description: `${
          images[0].score !== undefined ? `👍${images[0].score}` : ""
        } ${
          images[0].down_score === undefined || images[0].down_score === null
            ? ""
            : `👎${images[0].down_score}`
        } ${
          images[0].fav_count === undefined || images[0].fav_count === null
            ? ""
            : `⭐${images[0].fav_count}`
        }\n${images[0].tags.join(", ").substring(0, 200)}`,
        title: images[0].id || "",
        image: images[0].sampleUrl || images[0].previewURL || images[0].fileUrl,
        color: 0x975dc4,
        format: "large_image"
      };

      return base;
    }
  });
}
