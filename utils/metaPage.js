const escape = str =>
  str.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#x27;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\//g, '&#x2F;').replace(/\\/g, '&#x5C;').replace(/`/g, '&#96;');

export function metaPage(properties) {
    
  for(let prop in properties) {
    properties[prop] = escape(properties[prop])
  }
  
  return `
    <!DOCTYPE html>
    <html>
      <head>
        ${Object.entries(properties).map(query => {
          const [prop, value] = query
          return `<meta property="${prop.replace('.', ':')}" content="${value}">
                  ${prop !== "description" ? "" : `<meta name="description" content="${value}">`}`
        }).join('')}
      </head>
      <body>
        ${Object.entries(properties).map(query => {
          const [prop, value] = query
          return `<pre><code>property="${prop.replace('.', ':')}" content="${value}" </code></pre>`     
        }).join('')}
      </body>
    </html>
  `}