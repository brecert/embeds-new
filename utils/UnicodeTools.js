function inRange(number, range) {
  return range[0] <= number && number <= range[1]
}

class UnicodeTools {
  // offset: [from, to]

  static format(text, options) {

    options = {...{
      bold: true,
      italic: true,
      "font-face": "Sans-Serif"
    }, ...options}

    let res = []

    const offsets = () => {
      const uppercase = [0x0041, 0x005A]
      const lowercase = [0x0061, 0x007A]
      const numbers   = [0x0030, 0x0039]
      
      const alpha = (start) => {
        let ranges = {}
        ranges[start] = uppercase
        ranges[start + 26] = lowercase

        return ranges
      }
      
      return {
        "Bold": {
          "Serif": {
            ...alpha(0x1D400), 
            ...{ 0x1D7CE: numbers }
          },
          "Sans-Serif": {
            ...alpha(0x1D5D4),
            ...{ 0x1D7EC: numbers }
          }
        },
        "Italic": {
          "Serif": {
            ...alpha(0x1D435),
            ...{ 0x1D7CE: numbers }
          },
          "Sans-Serif": {
            ...alpha(0x1D608),
            ...{ 0x1D7E2: numbers }
          }
        },
        "BoldItalic": {
          "Serif": {
            ...alpha(0x1D468),
            ...{ 0x1D7CE: numbers }
          },
          "Sans-Serif": {
            ...alpha(0x1D63C),
            ...{ 0x1D7EC: numbers }
          }
        }
      }
    }

    for(let i = 0; i < text.length; i++) {
      let code = text.charCodeAt(i)
      let offset_range

      let choose = ""
      if(options.bold) choose = "Bold"
      if(options.italic) choose += "Italic"
      
      offset_range = offsets()[choose][options["font-face"]]

      for(let range in offset_range) {
        if(inRange(code, offset_range[range])) {
          code = Number(range) + (code - offset_range[range][0])
          break;
        }
      }

      res.push(code)
    }

    return res.map(code => String.fromCodePoint(code)).join('') 
  }
}

UnicodeTools.bold = function(text, font = "Sans-Serif") {
  return UnicodeTools.format(text, {bold: true, italic: false, "font-face": font})
}

UnicodeTools.italic = function(text, font = "Sans-Serif") {
  return UnicodeTools.format(text, {bold: false, italic: true, "font-face": font})
}

export default UnicodeTools