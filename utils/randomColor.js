export function randomColor(color, amount = 5) {
  return "#" + Math.floor((color + ((Math.random()*(1<<16)|0) / amount))).toString(16)
}